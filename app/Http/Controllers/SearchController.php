<?php

namespace App\Http\Controllers;

use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Null_;

class SearchController extends Controller
{
    /**
     * Method that calls view
     *
     * @param null $selectedTopic
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index($selectedTopic = null)
    {

        $articles = null;

        if (!is_null($selectedTopic)) {

            $url = $this->getTopic($selectedTopic);

            $response = $this->searchArticles($url);

            $articles = $response ? $this->convertCurl($response) : false;

            if (!$articles) {
                return redirect()->back()->withErrors(['error', 'Não foi possível realizar o processo!']);
            }
        }

        return view('index', compact('articles'));

    }

    /**
     * Method that receives filter by javascript
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOption(Request $request)
    {
        return $this->index($request->option);
    }

    /**
     * Return url by filter
     *
     * @param $topic
     * @return mixed
     *
     */
    public function getTopic($topic)
    {

        $topicOptions = [
            'life' => "https://www.sciencenews.org/topic/life",
            'tech' => "https://www.sciencenews.org/topic/tech",
        ];

        return $topicOptions[$topic];

    }

    /**
     * This method  get informations from url
     *
     * @param $url
     * @return bool|string
     *
     */
    public function searchArticles($url)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $errors   = curl_error($curl);
        curl_close($curl);

        if ($errors) {

            Log::error('Erros encontrados: ' . $errors);
            return false;

        } else {

            return $response;

        }
    }

    /**
     * Method that get values from resource
     *
     * @param $urlCurl
     * @return array
     */
    public function convertCurl($urlCurl)
    {

        $dom        = new DOMDocument();
        @$dom->loadHTML($urlCurl);
        $articles   = array();
        $xpath      = new \DOMXPath($dom);
        $html       = $xpath->query('//li[@class="post-item-river__wrapper___2c_E- with-image"]');

        foreach ($html as $key => $p){

            $status = $xpath->query('//h3[@class="post-item-river__title___J3spU"]');
            $time = $xpath->query('//time[@class="post-item-river__date___1Dcq1 entry-date published"]');

            $description = $status->item($key)->nodeValue;
            $date        = $time->item($key)->getAttribute('datetime');
            $url         = $status->item($key)->childNodes->item(1)->getAttribute('href');

            $class = new \stdClass();

            $class->url         = $url;
            $class->description = trim($description);
            $class->published   = date("d/m/Y", strtotime($date));

            array_push($articles, $class);

        }

        return $articles;
    }
}
