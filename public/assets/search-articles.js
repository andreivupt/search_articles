$(document).ready(function(){

    $("select.articles").change(function(){

        var option = $(this).children("option:selected").val();

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            data: option,
            url: 'http://searcharticles.dev.com/topic/' + option,
            success: function(data) {
                //console.log(data);
                $("body").html(data);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});

